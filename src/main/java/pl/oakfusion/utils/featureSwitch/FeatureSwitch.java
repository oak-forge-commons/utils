package pl.oakfusion.utils.featureSwitch;

public interface FeatureSwitch {

    boolean isActive(Feature feature);

    default <T> T getActiveImpl(Feature feature, T currentActiveImpl, T currentInactiveImpl) {
        if (isActive(feature)) {
            return currentActiveImpl;
        } else {
            return currentInactiveImpl;
        }
    }
}
