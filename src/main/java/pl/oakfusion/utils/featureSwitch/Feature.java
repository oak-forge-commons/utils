package pl.oakfusion.utils.featureSwitch;

import lombok.*;

public enum Feature {
    TREE_WALK(false),
    HANDLERS(false);

    @Getter
    @Setter
    private Boolean value;

    Feature(boolean value) {
        this.value = value;
    }

    public static boolean isValidEnum(String enumName) {
        if (enumName == null) {
            return false;
        }
        try {
            Feature.valueOf(enumName);
            return true;
        } catch (IllegalArgumentException ex) {
            return false;
        }
    }
}
