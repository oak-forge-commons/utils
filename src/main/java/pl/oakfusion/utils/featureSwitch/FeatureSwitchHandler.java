package pl.oakfusion.utils.featureSwitch;

import java.io.*;
import java.util.*;

public class FeatureSwitchHandler implements FeatureSwitch {
    private String propertiesFile = "features.properties";

    public FeatureSwitchHandler() {
        loadProperties();
    }

    public FeatureSwitchHandler(String propertiesFileName) {
        this.propertiesFile = propertiesFileName;
        loadProperties();
    }

    private void loadProperties() {
        Properties properties = new Properties();

        try (InputStream fileInputStream = getClass().getClassLoader().getResourceAsStream(propertiesFile)) {
            properties.load(fileInputStream);
            EnumSet enums = EnumSet.allOf(Feature.class);
            for (String key : properties.stringPropertyNames()) {
                String value = properties.getProperty(key);
                if (enums.contains(Feature.valueOf(key))) {
                    Feature.valueOf(key).setValue(Boolean.valueOf(value));
                }
            }
        } catch (NullPointerException | IllegalArgumentException e) {
            throw new IllegalStateException();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isActive(Feature feature) {
        loadProperties();
        return feature.getValue();
    }
}
