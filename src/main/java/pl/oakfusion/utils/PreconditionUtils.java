package pl.oakfusion.utils;

import lombok.NonNull;

public class PreconditionUtils extends RuntimeException {

	public static <T extends @NonNull Object> T checkNotNull(T reference) {
		if (reference == null) {
			throw new IllegalArgumentException();
		}
		return reference;
	}
}
