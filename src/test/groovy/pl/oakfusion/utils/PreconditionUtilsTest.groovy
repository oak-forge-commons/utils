package pl.oakfusion.utils

import spock.lang.Specification

class PreconditionUtilsTest extends Specification {

    def 'should throw IllegalArgumentException'(){
        when:
        PreconditionUtils.checkNotNull(null)

        then:
        thrown(IllegalArgumentException)
    }

    def 'should return given object'(){
        given:
        PreconditionUtils preconditionUtils = new PreconditionUtils()

        when:
        def obj = PreconditionUtils.checkNotNull(preconditionUtils)

        then:
        noExceptionThrown()
        obj instanceof PreconditionUtils
    }
}
