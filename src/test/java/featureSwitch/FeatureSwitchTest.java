package featureSwitch;

import org.junit.jupiter.api.Test;
import pl.oakfusion.utils.featureSwitch.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

class FeatureSwitchTest {

    @Test
    void should_return_first_implementation() {
        //given..
        FeatureSwitch featureSwitch = new FeatureSwitchHandler();

        //when..
        TestInterface testInterface = featureSwitch.getActiveImpl(Feature.TREE_WALK, new TestInterfaceImplOne(), new TestInterfaceImplTwo());

        //then..
        assertThat(testInterface).isInstanceOf(TestInterfaceImplOne.class);
    }

    @Test
    void should_fail_on_bad_property_in_file() {
        //given..

        //when..

        //then..
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() ->
                new FeatureSwitchHandler("featuresTest.properties"));
    }

    @Test
    void should_fail_on_wrong_file_path() {
        //given..

        //when..

        //then..
        assertThatExceptionOfType(IllegalStateException.class).isThrownBy(() ->
                new FeatureSwitchHandler("testfile.properties"));
    }

}
